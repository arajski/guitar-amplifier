from pyaudio import PyAudio, paContinue, paInt32
from numpy import fft, fromstring, int32
from math import floor
from time import sleep
#
############## CONSTANTS ########################
#

SAMPLE_RATE = 44100
CHANNELS    = 1
BUFFER_SIZE = 16

#
############## AMPLIFIER PROCESSING #############
#

audio = PyAudio()

def callback(data, frame_count, time_info, status):   
    data = fromstring(data, dtype=int32)
    freq_data = fft.rfft(data)
    
    eq_ratio = int(floor(freq_data.size/3))
    bass_ratio = 0
    mid_ratio = eq_ratio 
    high_ratio = 2*eq_ratio 

    for i in range(bass_ratio,mid_ratio):
        freq_data[i] = 0 #Implement eq function
    
    inv_freq_data = fft.irfft(freq_data)
    out_data = inv_freq_data.astype(int32).tostring()
    return out_data, paContinue

#
############## AUDIO STREAMING ###################
#
stream = audio.open(format=paInt32,
                channels=CHANNELS,
                rate=SAMPLE_RATE,
                frames_per_buffer = BUFFER_SIZE,
                input=True,
                output=True,
                stream_callback=callback)

stream.start_stream()

while stream.is_active():
    sleep(0.1)

stream.stop_stream()
stream.close()

audio.terminate()
